package main

import (
	"net/http"
	"strings"
	"crypto/sha256"
	"google.golang.org/appengine"
	"encoding/hex"
)

func catchAllFileServer(fs http.Handler) http.Handler {
	staticPaths := [...]string{
		"/static/",
		"/img/",
		"/font/",
		"/sitemap.xml",
        "/service-worker.js",
        "/asset-manifest.json",
	}
	fn := func(w http.ResponseWriter, req *http.Request) {
		var isStaticPath bool = false
		for _, path := range staticPaths {
			if strings.HasPrefix(req.URL.Path, path) {
				isStaticPath = true
				break
			}
		}

		ctx := appengine.NewContext(req)
		sum := sha256.Sum256([]byte(appengine.VersionID(ctx)))

		w.Header().Set("ETag", string(hex.EncodeToString(sum[:])))
		w.Header().Set("Cache-Control", "Cache-Control: public, max-age=31536000")
		if isStaticPath {
			fs.ServeHTTP(w, req)
		} else {
			fsHandler := http.StripPrefix(req.URL.Path, fs)
			fsHandler.ServeHTTP(w, req)
		}
	}
	return http.HandlerFunc(fn)
}

func init() {
	fs := http.FileServer(http.Dir("web"))
	http.Handle("/", catchAllFileServer(fs))
}
