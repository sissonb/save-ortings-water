import React, {Component} from 'react';
import pageTracking from '../../utility/pageTracking';
import ReactTable from "react-table";
import "react-table/react-table.css";

class Traffic extends Component {
    componentDidMount() {
        pageTracking('Traffic');
    }

    render() {
        return (
            <article id="TrafficPage">
                <h2>Traffic</h2>
                <p>The addition of a gravel pit in the area will increase the number of large dump trucks barrelling
                    down Orville road. Orville road is already the most dangerous intersection off of SR 162.</p>
                <p>The following information was gathered from <em>Orting Valley Fire and Rescue</em> over the past 5
                    years and shows that the Orville road intersection is the largest contributor to accidents along SR
                    162 </p>
                <ReactTable
                    showPagination={false}
                    defaultPageSize={13}
                    data={[{
                        streetName: "Orville",
                        numOfAccidents: 45,
                        percentOfAccidents: 26.47,
                    }, {
                        streetName: "136th",
                        numOfAccidents: 15,
                        percentOfAccidents: 8.82,
                    }, {
                        streetName: "Military",
                        numOfAccidents: 13,
                        percentOfAccidents: 7.65,
                    }, {
                        streetName: "128th",
                        numOfAccidents: 13,
                        percentOfAccidents: 7.65,
                    }, {
                        streetName: "Williams",
                        numOfAccidents: 12,
                        percentOfAccidents: 7.06,
                    }, {
                        streetName: "149th/150th",
                        numOfAccidents: 10,
                        percentOfAccidents: 5.88,
                    }, {
                        streetName: "Patterson",
                        numOfAccidents: 7,
                        percentOfAccidents: 4.12,
                    }, {
                        streetName: "144th",
                        numOfAccidents: 7,
                        percentOfAccidents: 4.12,
                    }, {
                        streetName: "115th",
                        numOfAccidents: 6,
                        percentOfAccidents: 3.53,
                    }, {
                        streetName: "110th/109th/108th",
                        numOfAccidents: 6,
                        percentOfAccidents: 3.53,
                    }, {
                        streetName: "146th",
                        numOfAccidents: 4,
                        percentOfAccidents: 2.35,
                    }, {
                        streetName: "106th",
                        numOfAccidents: 1,
                        percentOfAccidents: 0.59,
                    }, {
                        streetName: "1022nd",
                        numOfAccidents: 1,
                        percentOfAccidents: 0.59,
                    }]}
                    columns={[
                        {
                            Header: "Summary of Intersection Accidents Along SR 162",
                            columns: [
                                {
                                    Header: "Street Name",
                                    accessor: "streetName"
                                },
                                {
                                    Header: "Accident #",
                                    accessor: "numOfAccidents"
                                },
                                {
                                    Header: "Accident %",
                                    accessor: "percentOfAccidents",
                                    Cell: row => (`${row.value}%`)
                                }
                            ]
                        }
                    ]}
                    className="-striped -highlight"
                />
            </article>
        );
    }
}

export default Traffic;