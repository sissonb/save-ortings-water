import React, {Component} from 'react';
import pageTracking from '../../utility/pageTracking';

class Home extends Component {
    componentDidMount() {
        pageTracking('Home');
    }

    render() {
        return (
            <article id="HomePage">
                <h2>Save Orting's Water!</h2>
                <p>Azure Green Consultants have submitted to Pierce County to lift a 6 year development moratorium on 5
                    separate parcels (totaling 95.51 acres) to begin the permit process for a sand and rock quarry at
                    the top of Fisk Road. Allowing this would jeopardize Fiske Creek, the Puyallup River, our aquifer,
                    wells, and the <b>City of Orting’s spring fed water tower</b> on Fisk Road.</p>
                <p>Concerned residents of Orting have created a 501(c)(3) nonprofit to fight back against this rock
                    quarry development. Donations to the nonprofit will go towards hiring professionals to do
                    environmental studies that prove the negative effects of a rock quarry near Orting's water source
                    and to do what's necessary to prevent hazardous developments in the area.</p>
                <p className="align-center"><a target="_blank"
                                               href="https://www.paypal.com/us/webapps/mpp/donations"><img alt="Donate"
                                                                                                           src="/img/donate.png"/></a>
                </p>
                <p>In addition to the nonprofit organization, we've created an online petition to get the word out about
                    the threat to Orting's water. Please add your name to the petition to voice your opposition to this
                    rock quarry </p>
                <p className="align-center"><a
                    href="https://www.change.org/p/pierce-county-save-orting-s-water"
                    target="_blank"><img width="330" alt="Sign Our Petition on Change.org" src="/img/sign-petition.jpg"/></a></p>
            </article>
        );
    }
}

export default Home;
