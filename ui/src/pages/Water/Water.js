import React, {Component} from 'react';
import pageTracking from '../../utility/pageTracking';

class Water extends Component {
    componentDidMount() {
        pageTracking('Water');
    }

    render() {
        return (
            <article id="WaterPage">
                <h2>Water</h2>
                <p>
                    The city of Orting published a <a
                    href="http://cityoforting.org/wp-content/uploads/2017/06/CCR-for-Reporting-Year-2016-report.pdf"
                    target="_blank">water quality report back in June 2017</a> and it states that the city's spring fed
                    water sources are highly susceptibility to groundwater contamination.
                </p>
                <blockquote>
                    The purpose of a wellhead protection program is to provide local utilities with a
                    proactive program for preventing groundwater contamination. One of the major components of a
                    successful plan is a susceptibility rating.
                </blockquote>
                <blockquote>...the
                    three City spring sources rate high.
                </blockquote>
                <p>
                    This rock quarry is a threat to our water supply in Orting. They plan to replace the rock they
                    excavated from the area near Orting's spring fed water tower with <b>solid waste</b> which
                    will <b>contaminate the city's water supply</b>. Also any digging in the area may cause the aquifers
                    to drain, lowering the supply of water to the city of Orting and leaving many homes in the area
                    without water.
                </p>
            </article>
        );
    }
}

export default Water;