import React, {Component} from 'react';
import pageTracking from '../../utility/pageTracking';

class Landslide extends Component {
    componentDidMount() {
        pageTracking('Landslide');
    }

    render() {
        return (
            <article id="LandslidePage">
                <h2>Landslide</h2>
                <p>Soil maps of the area provided by Washington Department of Natural Resources indicate that the soil
                    at the proposed gravel pit has been labeled "Highly Erodible". A gravel quarry is negligent and
                    irresponsible considering <a href="https://www.dnr.wa.gov/publications/ger_fs_union_gap.pdf"
                                                 target="_blank">recent quarry involved landslides</a>.</p>

            </article>
        );
    }
}

export default Landslide;
