import React from 'react'

import Header from './components/Header/Header';
import Main from './components/Main/Main';
import Navigation from './components/Navigation/Navigation';

class App extends React.Component {
    render() {
        document.body.style.minHeight = "100vh";
        return (
            <div id="App">
                <Header ref="header"/>
                <Navigation/>
                <Main/>
            </div>
        )
    }
}
export default App;