import React from 'react';
import outboundTracking from '../../utility/outboundTracking';

class SocialLinks extends React.Component {
    render() {
        return (
            <nav id="SocialNetworkLinks">
                <div className="container">
                    <div>
                        <p>
                            <a onClick={outboundTracking.bind(this, 'Facebook')}
                              href="https://www.facebook.com/redhouserenton/">
                                <img height="50" src="/img/facebook.png" alt="facebook icon"/>
                            </a>
                        </p>
                    </div>
                    <div>
                        <p>
                            <a onClick={outboundTracking.bind(this, 'Open Table')}
                               href="https://www.opentable.com/r/red-house-reservations-renton?restref=61378&lang=en">
                                <img height="50" src="/img/open-table.png" alt="opentable icon"/>
                            </a>
                        </p>
                    </div>
                    <div>
                        <p>
                            <a onClick={outboundTracking.bind(this, 'Yelp')}
                               href="https://www.yelp.com/biz/red-house-beer-and-wine-shoppe-renton">
                                <img height="50" src="/img/yelp.png" alt="yelp icon"/>
                            </a>
                        </p>
                    </div>
                    <div>
                        <p>
                            <a onClick={outboundTracking.bind(this, 'Twitter')}
                                href="https://twitter.com/redhouse_renton">
                                <img height="50" src="/img/twitter.png" alt="yelp icon"/>
                            </a>
                        </p>
                    </div>
                    <div>
                        <p>
                            <a onClick={outboundTracking.bind(this, 'Instagram')}
                                href="https://www.instagram.com/redhouserenton/">
                                <img height="50" src="/img/instagram.png" alt="Instagram icon"/>
                            </a>
                        </p>
                    </div>
                </div>
            </nav>
        );
    }
}

export default SocialLinks;
