import React from 'react';
import {Link} from 'react-router-dom';
import {withRouter} from 'react-router-dom';

class Navigation extends React.Component {
    toggleMenu() {
        this.refs.menuList.classList.toggle("is-active");
        this.refs.menuBurger.classList.toggle("is-active");
    }

    closeMenu() {
        this.refs.menuList.classList.remove("is-active");
        this.refs.menuBurger.classList.remove("is-active");
    }

    render() {
        const pathname = this.props.location.pathname;
        return (
            <nav id="Navigation">
                <ul ref="menuList">
                    <li className={(pathname === '/') ? 'active' : '' }>
                        <Link to="/" onClick={this.closeMenu.bind(this)}>Home</Link>
                    </li>
                    <li className={(pathname === '/water/') ? 'active' : '' }>
                        <Link to="/water/" onClick={this.closeMenu.bind(this)}>Water</Link>
                    </li>
                    <li className={(pathname === '/traffic/') ? 'active' : '' }>
                        <Link to="/traffic/" onClick={this.closeMenu.bind(this)}>Traffic</Link>
                    </li>
                    <li className={(pathname === '/landslide/') ? 'active' : '' }>
                        <Link to="/landslide/" onClick={this.closeMenu.bind(this)}>Landslide</Link>
                    </li>
                </ul>
                <div ref="menuBurger" className="hamburger hamburger--spin" onClick={this.toggleMenu.bind(this)}>
                    <div className="hamburger-box">
                        <div className="hamburger-inner"/>
                    </div>
                </div>
            </nav>
        );
    }
}

export default withRouter(Navigation);