import React from 'react';
import { Route } from 'react-router-dom';
import Home from '../../pages/Home/Home';
import Traffic from '../../pages/Traffic/Traffic';
import Water from '../../pages/Water/Water';
import Landslide from '../../pages/Landslide/Landslide';

class Main extends React.Component {
   render() {
        return (
            <main id="Main">
                <Route exact path='/' component={Home}/>
                <Route exact path="/traffic/" component={Traffic} />
                <Route exact path="/water/" component={Water}/>
                <Route exact path="/landslide/" component={Landslide}/>
            </main>
        );
    }
}

export default Main;
