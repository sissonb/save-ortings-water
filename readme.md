# Save Orting's Water Google App Engine Server

## Dependencies

### Google App Engine\(GAE\) Account
Sign up for a Google App Engine account 
https://cloud.google.com/appengine/

### Create a GAE project
Create a project to hold the red house website
https://console.cloud.google.com/appengine

### Install the Google App Engine SDK
Follow steps 1 and 2. 

https://cloud.google.com/appengine/docs/standard/go/download

### Init gcloud
Open the command prompt and run the command `gloud init`. Log in with the GAE account that has the project. 

## Deploying the website
1. Follow the instruction in the `/ui/` folder to create a website build
1. Copy the files from the `/ui/build/` folder to `/src/save-ortings-water/web/` folder
    * Copy the folders and files **in the folder**, not the `/ui/build/` folder itself
1. Test on localhost 
    1. Open the command prompt and navigate to the root of this project
    1. Run the `goapp serve ./src/save-ortings-water/app.yaml` command
    1. Open a web browser and navigate to http://localhost:8080
1. Deploy to Google App Engine
    1. Run `gcloud app deploy ./src/save-ortings-water/` command from the root of this project
1. Check Google App Engine's [Version Page](https://console.cloud.google.com/appengine/versions) to verify the new version deployed

## Reverting changes
1. Go to Google App Engine's [Version Page](https://console.cloud.google.com/appengine/versions)
1. Check the previous version
1. Click *Migrate Traffic*

## Deleting Version
Every time the website is deployed a new version is created. The limit for versions is 15. Deploys will fail once the limit is reached. Delete old version to restore the ability to deploy new changes    

1. Go to the [Version Page](https://console.cloud.google.com/appengine/versions) 
1. Select some older versions and then click the trash button.
    * Do not delete the version with 100% traffic allocation
 